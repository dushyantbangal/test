Create the following file at any location you desire:

```
#!/bin/sh -

cd PATH_TO_GATEWAY_DIRECTORY
sudo NODE_ENV=production node server.js

```

Give it executable permission
`sudo chmod +x FILE_NAME`

Now create a systemd service file in `/etc/systemd/system` with the name `startApi.service`

Paste following contents in the service file:

```
[Unit]
Description=Start Winjit IoT GDM/RCM
After=mongodb.service

[Service]
ExecStart=PATH_TO_EXECUTION_SCRIPT
Restart=always
RestartSec=30

[Install]
WantedBy=multi-user.target

```

Start service:
`sudo systemctl start startApi`

Check service status:
`sudo systemctl status startApi`

Enable service (auto run on reboot):
`sudo systemctl enable startApi`